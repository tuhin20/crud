<?php  

class ProductModel extends CI_Model {

public function get_all_product(){

$this->db->select('*');
$this->db->from('products');
$query=$this->db->get();
$result=$query->result_array();
 return $result;


}


 public function add_product($data)
{
   $this->db->insert('products',$data);
   return $this->db->insert_id(); 
}

public function delete($id){

$this->db->from('products');
$this->db->where('id',$id);
$this->db->delete();

}
public function get_product_by_id($id)
{
 $this->db->select('*');
$this->db->from('products');
$this->db->where('id',$id);
$query=$this->db->get();
$result=$query->result_array();
 return $result;
}

 public function update_product_by_id($data,$id)
{
   $this->db->where('id', $id);
   $this->db->update('products', $data);
    
}

public function insertCSV($data)
 {
    $this->db->insert('products', $data);
    return $this->db->insert_id();
 }
 public function changestatus($id)
{
	 $this->db->select('status');
$this->db->from('products');
$this->db->where('id',$id);
$query=$this->db->get();
$result=$query->row_array();

if ($result['status']==1) {
	$result['status']=0;
}elseif ($result['status']==0) {
	$result['status']=1;
}

   $this->db->where('id', $id);
   $this->db->update('products', $result);
}


} 