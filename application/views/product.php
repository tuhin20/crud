
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <!-- //databale -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <a href="ProductController/add_product" class="btn btn-primary">Add</a>    
  <a href="LoginController/logout" class="btn btn-primary">Logout</a> 

  <form class="form-horizontal well" action="<?php echo base_url(); ?>ProductController/import" method="post"  enctype="multipart/form-data">
<input type="file" name="file" id="file" class="input-large">
<button type="submit" id="submit" name="Import" class="btn btn-primary button-loading">Upload</button>
</form>
<div>
  <form action="SmsapiController/sendsms" method="post">
<div class="container">
  <h2>Send Message</h2>
  <p id="message"></p>
  <div class="form-group">
       <label for="email">Sender ID:</label>
       <input type="number" class="form-control" id="senderid" placeholder="Enter Sender ID" name="senderid">
    </div>
    <div class="form-group">
    <label for="pwd">Mobile No.:</label>
    <input type="text" class="form-control" id="contacts" placeholder="Enter Mobile No" name="contacts">
    </div>
    <div class="form-group">
    <label for="comment">Message:</label>
    <textarea class="form-control" rows="5" id="msg" name="msg" placeholder="Write Your Message Here...."></textarea>
  </div>
    <button type="submit" class="btn btn-primary" id="send">Send Message</button>
 
</div>
</div>
<a id="approve" class="btn btn-primary">Change Status</a>
  <table  class="table table-striped" id="myTable">

    <thead>

      <tr>
       
        <th><input type="checkbox" name="chk[]" value="0"/></th>
        <th>SL</th>
        <th>Name</th>
        <th>Code</th>
        <th>Category</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    
      
    
    <tbody>
      <?php foreach ($productlist as $key => $value) { ?>

      <tr>
        
        <td><input class="status" type="checkbox"  value="<?php echo $value['id'] ?>"/></td>
        <td><?php echo $value['id'] ?></td>
        <td><?php echo $value['name'] ?></td>
        <td><?php echo $value['code'] ?></td>
        <td><?php echo $value['category'] ?></td>
         <td><?php if($value['status']==1) {
           echo "Approved";
         } else
         { echo "Pending"; 
                   }
         ?></td>
        <td>
          <a href='<?php echo site_url('ProductController/edit/'.$value['id'])?>' class="btn btn-primary">Edit</a>
          <a href='<?php echo site_url('ProductController/delete/'.$value['id'])?>' class="btn btn-danger">Delete</a>
        </td>
       

      </tr>
      <?php } ?>
      
    </tbody>
  </table>
</div>

</body>

<script type="text/javascript">
  $(document).ready( function () {
    $('#myTable').DataTable();

} );
</script>
<script type="text/javascript">
 

  $( "#approve" ).click(function() {
     
     var id = [];
        $('.status:checked').each(function(i){
          id[i] = $(this).val();
        });

     $.ajax({
  type: "POST",
  url: '<?php echo base_url(); ?>ProductController/changestatus',
  data: { id : id },
  success: function(msg){
     window.location.reload();
   }
})
  

  
});

</script>
</html>
