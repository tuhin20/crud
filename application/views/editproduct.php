<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Add Product</h2>
  <form class="form-horizontal" method="Post" action="<?php echo site_url('ProductController/update');?>">
    <div class="form-group">
      <label class="control-label col-sm-2" for="name">Name:</label>
      <div class="col-sm-10">
        <input type="text" value=" <?php echo $productinfo[0]['name'] ?>" class="form-control" id="name" placeholder="Enter Name" name="name">
        <input type="hidden" value=" <?php echo $productinfo[0]['id'] ?>" class="form-control" id="id" name="id">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="code">Code:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" value=" <?php echo $productinfo[0]['code'] ?>" id="code" placeholder="Enter code" name="code">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="category">Category:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" value=" <?php echo $productinfo[0]['category'] ?>" id="category" placeholder="Enter Category" name="category">
      </div>
    </div>
   
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Update</button>
      </div>
    </div>
  </form>
</div>

</body>
</html>
