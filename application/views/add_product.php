<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Add Product</h2>
  <form class="form-horizontal" method="Post" action="create" enctype="multipart/form-data">
    <div class="form-group">
      <label class="control-label col-sm-2" for="name">Name:</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="code">Code:</label>
      <div class="col-sm-6">          
        <input type="text" class="form-control" id="code" placeholder="Enter code" name="code">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="category">Category:</label>
      <div class="col-sm-6">          
        <input type="text" class="form-control" id="category" placeholder="Enter Category" name="category">
      </div>
    </div>  
      <div class="form-group">
      <label class="control-label col-sm-2" for="category">Image Upload:</label>
      <div class="col-sm-6">          
        <input type="file" class="form-control" id="category" placeholder="Enter Category" name="file">
      </div>
    </div>
  

     <div class="form-group">
      <label class="control-label col-sm-2" for="status">Status:</label>
      <div class="col-sm-6">          
        <input type="text" class="form-control" id="status" placeholder="Enter Status" name="status">
      </div>
    </div>
       <div class="form-group">
      <label class="control-label col-sm-2" for="category">Type:</label>
      <select name="type" id="type" class="col-sm-2">
  <option value="New">New</option>
  <option value="Existing">Existing</option>
 
</select>
    </div>
     <div class="form-group abcd" style="display: none;">
      <label class="control-label col-sm-2" for="category">Existing:</label>
      <div class="col-sm-6">          
        <select name="type" id="type" class="col-sm-2">
        <?php foreach ($productlist as $key => $value) { ?>
        <option value="<?php echo $value['name']; ?>"><?php echo $value['name']; ?></option>
      <?php  } ?>

  
 
</select>
      </div>
    </div>
    <!--  <div class="form-group abcd" style="display: none;">
      <label class="control-label col-sm-2" for="category">Existing:</label>
      <div class="col-sm-6">          
        <input type="text" class="form-control" id="category" placeholder="Enter Category" name="ext">
      </div>
    </div> -->
   
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript">
  $('#type').on('change',function(){
    type = $(this).val();
    if(type == 'Existing'){
        $('.abcd').show();
    }else{
      $('.abcd').hide();
    }
  });

</script>
</body>
</html>
