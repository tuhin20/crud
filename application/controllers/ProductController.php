<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('ProductModel'); 
        $userid=$this->session->userdata('userid');
        $email=$this->session->userdata('email');
        if (!$userid) {
        	redirect('LoginController');
        }

    }


	public function index()
	{
		$data['productlist']=$this->ProductModel->get_all_product();
		/*echo '<pre>';p
		print_r($data);*/
		$this->load->view('product',$data);
	}
	public function productname()
	{
		$data['productlist']=$this->ProductModel->get_all_product();
		/*echo '<pre>';
		print_r($data);die();*/
		$this->load->view('add_product',$data);
	}
	public function add_product()
	{
		
		$this->load->view('add_product');
	}

	public function create()
	{
	 $data['name']=	$this->input->post('name');
	 $data['code']=	$this->input->post('code');
	 $data['category']=	$this->input->post('category');
	 $image=$_FILES['file']['name'];
	 $data['image']=$image;
	/* echo "<pre>"; print_r($image);
	 die();*/
	 if ($image) {
	 	move_uploaded_file($_FILES['file']['tmp_name'], 'asset/img/' . $image);
	 	 /*$config['upload_path'] = './asset/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 2000;
        $config['max_width'] = 1500;
        $config['max_height'] = 1500;*/

       // $this->load->library('upload', $config);

        /*if (!$this->upload->do_upload('profile_pic')) 
		{
            //$error = array('error' => $this->upload->display_errors());
            echo "image uploaded failed";
           
        } 
		else 
		{
            $data = array('image_metadata' => $this->upload->data());

            echo "image uploaded succesfully";
        }*/
	 }
	

	 $type=	$this->input->post('type');
	 if ( $type=='New') {
	 	 $data['type']=	$this->input->post('type');
	 }else{
	 	 $data['type']=	$this->input->post('ext');
	 }
     $id = $this->ProductModel->add_product($data);
   
     return  redirect('ProductController');
	}
	public function edit($id)
	{
     $data['productinfo']=$this->ProductModel->get_product_by_id($id);
     $this->load->view('editproduct',$data);
	}

	public function delete($id)
	{
     $this->ProductModel->delete($id);
     
     redirect('ProductController');
	}
	public function update()
	{
     
	 $id=$this->input->post('id');
	 $data['name']=	$this->input->post('name');
	 $data['code']=	$this->input->post('code');
	 $data['status']=	$this->input->post('status');
	 $data['category']=	$this->input->post('category');
     $this->ProductModel->update_product_by_id($data,$id);
	 return  redirect('ProductController');
	}
	public function import()
        {
    

          if(isset($_POST['Import']))
            {
                $filename=$_FILES["file"]["tmp_name"];
                 /*echo '<pre>'; print_r($filename);
                  die();*/

                if($_FILES["file"]["size"] > 0)
                  {
                    $file = fopen($filename, "r");
                     while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
                     {
                            $data = array(
                                'name' => $emapData[1],
                                'code' => $emapData[2],
                                'category' => $emapData[3],
                              
                                );

                            /*echo '<pre>'; print_r($data);
                            die();*/

                        $insertId = $this->ProductModel->insertCSV($data);
                     }
                    fclose($file);
                     redirect('ProductController');
                  }
            }
       }   

        public function changestatus(){
		$id =	$this->input->post('id');

		foreach ($id as $key => $value) {
			$this->ProductModel->changestatus($value);
		}

		return ;



	}

	public function customer(){
		$this->load->view('customer');
	}

	public function smsapi($apiKey='',$number='')
	{
		$authKey = '123456';
		if ($authKey != $apiKey) {
		   echo "invalid access token";
		   exit();
		}
		$this->load->view('add_product');
		//echo "sms send";
	}
 }
