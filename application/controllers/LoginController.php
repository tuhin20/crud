<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
    }

    public function index()
	{
		
		$this->load->view('login');
	}
	public function login()
	{
		

	    $email=	$this->input->post('email');
	    $password=	$this->input->post('password');
	    //echo '<pre>'; print_r($email);
	    //echo '<pre>'; print_r($password);
	    // die();
	     $user=$this->UserModel->checkuser($email,$password);
	    /*echo '<pre>'; print_r($user[0]['userid']);
	    die();*/

	     
	      if ($user) {
	      	$session_data['userid'] = $user['userid'];
            $session_data['email'] =$user['email'];
            $this->session->set_userdata($session_data);
           
	      	redirect ('ProductController');
	      }
	      else{
	      	$this->session->set_userdata('Fail', 'Fail');
	      	redirect ('LoginController');
	      }
		
	}
	public function logout(){
		$this->session->sess_destroy();
		$this->session->set_userdata('Logout Successfull', 'logout');
		redirect ('LoginController');
	}
	
}